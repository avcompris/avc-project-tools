package net.avcompris.devops;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.apache.commons.lang3.StringUtils.substringBeforeLast;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeFalse;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import javax.annotation.Nullable;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.google.common.collect.Lists;

import net.avcompris.binding.dom.helper.DomBinderUtils;
import net.avcompris.project.Pom;
import net.avcompris.project.Pom.CiManagement;
import net.avcompris.project.Pom.Dependency;
import net.avcompris.project.Pom.Plugin;
import net.avcompris.project.Pom.Repository;
import net.avcompris.project.Pom.Site;

public class AllPomsInWorkspaceTest {

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testModelVersion(
		final String dirName,
		final File dir,
		final Pom pom
	) throws Exception {

		assertEquals("4.0.0", pom.getModelVersion());
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testArtifactIdIsProjectName(
		final String dirName,
		final File dir,
		final Pom pom
	) throws Exception {

		final String projectName = pom.getName();

		assertEquals(projectName, pom.getArtifactId());
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testProjectNameIsDirName(
		final String dirName,
		final File dir,
		final Pom pom
	) throws Exception {

		final String projectName = pom.getName();

		assertEquals(dirName, projectName);
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testReadmeHeading1IsProjectName(
		final String dirName,
		final File dir,
		final Pom pom
	) throws Exception {

		final String projectName = pom.getName();

		final File readmeFile = new File(dir, "README.md");

		assumeTrue(readmeFile.isFile());

		final String header1 = extractHeader1FromMarkdownFile(readmeFile);

		if (!header1.contentEquals("About " + projectName)) {

			assertEquals(projectName, header1);
		}
	}

	private static String extractHeader1FromMarkdownFile(
		final File mdFile
	) throws IOException {

		final String line0 = FileUtils.readLines(mdFile, UTF_8.name()).get(0);

		if (!line0.startsWith("# ")) {

			throw new RuntimeException("line0 should start with \"# \", but was: " + line0);
		}

		return substringAfter(line0, "# ").trim();
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testScmConnection(
		final String dirName,
		final File dir,
		final Pom pom
	) throws Exception {

		assumeNotModule(dir);

		final org.eclipse.jgit.lib.Repository gitRepository = new FileRepositoryBuilder()
				.setGitDir(new File(dir, ".git")).build();

		// e.g. "scm:git:git@gitlab.com:avcompris/avc-base-testutil.git"
		// e.g. "https://gitlab.com/avcompris/avc-base-testutil.git"
		// e.g.
		// "https://gitlab-ci-token:[MASKED]@gitlab.com/avcompris/avc-base-testutil.git"
		//
		final String originUrl = gitRepository.getConfig().getString("remote", "origin", "url");

		final String straightforwardUrl = "scm:git:" + removeHttpsCredentials(originUrl);

		final String scmConnection = pom.getScm().getConnection();

		if (originUrl.startsWith("git@gitlab.com:")) {

			final String suffix = substringAfter(originUrl, ":");

			final String webUrl = "scm:git:https://gitlab.com/" + suffix;

			if (scmConnection.equals(webUrl)) {

				return;
			}

			assertEquals(straightforwardUrl, scmConnection, "or: " + webUrl);
		}

		assertEquals(straightforwardUrl, scmConnection);
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testScmDeveloperConnection(
		final String dirName,
		final File dir,
		final Pom pom
	) throws Exception {

		assumeNotModule(dir);

		final org.eclipse.jgit.lib.Repository gitRepository = new FileRepositoryBuilder()
				.setGitDir(new File(dir, ".git")).build();

		// e.g. "scm:git:git@gitlab.com:avcompris/avc-base-testutil.git"
		// e.g. "https://gitlab.com/avcompris/avc-base-testutil.git"
		//
		final String originUrl = gitRepository.getConfig().getString("remote", "origin", "url");

		final String straightforwardUrl = "scm:git:" + removeHttpsCredentials(originUrl);

		final String scmDeveloperConnection = pom.getScm().getDeveloperConnection();

		if (originUrl.startsWith("git@gitlab.com:")) {

			final String suffix = substringAfter(originUrl, ":");

			final String webUrl = "scm:git:https://gitlab.com/" + suffix;

			if (scmDeveloperConnection.equals(webUrl)) {

				return;
			}

			assertEquals(straightforwardUrl, scmDeveloperConnection, "or: " + webUrl);

		} else if (originUrl.startsWith("https://")) {

			final String suffix = substringAfter(substringAfter(originUrl, "//"), "/");

			final String sshUrl = "scm:git:git@gitlab.com:" + suffix;

			if (scmDeveloperConnection.equals(sshUrl)) {

				return;
			}

			assertEquals(straightforwardUrl, scmDeveloperConnection, "or: " + sshUrl);
		}

		assertEquals(straightforwardUrl, scmDeveloperConnection);
	}

	@Test
	public void testRemoveHttpsCredentials() throws Exception {

		assertEquals("https://gitlab.com/avcompris/avc-base-testutil.git",
				removeHttpsCredentials("https://gitlab-ci-token:[MASKED]@gitlab.com/avcompris/avc-base-testutil.git"));

		assertEquals("https://gitlab.com/avcompris/avc-base-testutil.git",
				removeHttpsCredentials("https://gitlab.com/avcompris/avc-base-testutil.git"));

		assertEquals("scm:git:https://gitlab.com/avcompris/avc-base-testutil.git", removeHttpsCredentials(
				"scm:git:https://gitlab-ci-token:[MASKED]@gitlab.com/avcompris/avc-base-testutil.git"));

		assertEquals("scm:git:https://gitlab.com/avcompris/avc-base-testutil.git",
				removeHttpsCredentials("scm:git:https://gitlab.com/avcompris/avc-base-testutil.git"));

		assertEquals("scm:git:git@gitlab.com:avcompris/avc-base-testutil.git",
				removeHttpsCredentials("scm:git:git@gitlab.com:avcompris/avc-base-testutil.git"));
	}

	private static String removeHttpsCredentials(
		final String url
	) {

		if (url.startsWith("https://") && url.contains("@")) {

			return "https://" + substringAfter(url, "@");

		} else if (url.startsWith("scm:git:https://") && url.contains("@")) {

			return "scm:git:https://" + substringAfter(url, "@");

		} else {

			return url;
		}
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testScmUrl(
		final String dirName,
		final File dir,
		final Pom pom
	) throws Exception {

		assumeNotModule(dir);

		final org.eclipse.jgit.lib.Repository gitRepository = new FileRepositoryBuilder()
				.setGitDir(new File(dir, ".git")).build();

		// e.g. "scm:git:git@gitlab.com:avcompris/avc-base-testutil.git"
		// e.g. "https://gitlab.com/avcompris/avc-base-testutil.git"
		//
		final String originUrl = gitRepository.getConfig().getString("remote", "origin", "url");

		final String scmUrl = pom.getScm().getUrl();

		final String scmUrlRef;

		if (originUrl.startsWith("git@gitlab.com:")) {

			final String suffix = substringAfter(originUrl, ":");

			scmUrlRef = "https://gitlab.com/" + (suffix.endsWith(".git") //
					? substringBeforeLast(suffix, ".git") //
					: suffix);

		} else if (originUrl.startsWith("https://gitlab.com/")) {

			final String suffix = substringAfter(originUrl, "gitlab.com/");

			scmUrlRef = "https://gitlab.com/" + (suffix.endsWith(".git") //
					? substringBeforeLast(suffix, ".git") //
					: suffix);

		} else if (originUrl.startsWith("https://")) {

			scmUrlRef = originUrl.endsWith(".git") //
					? substringBeforeLast(originUrl, ".git") //
					: originUrl;

		} else {

			throw new NotImplementedException("originUrl: " + originUrl);
		}

		assertEquals(removeHttpsCredentials(scmUrlRef), scmUrl);
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testUrl(
		final String dirName,
		final File dir,
		final Pom pom
	) throws Exception {

		assumeNotModule(dir);

		final org.eclipse.jgit.lib.Repository gitRepository = new FileRepositoryBuilder()
				.setGitDir(new File(dir, ".git")).build();

		// e.g. "scm:git:git@gitlab.com:avcompris/avc-base-testutil.git"
		// e.g. "https://gitlab.com/avcompris/avc-base-testutil.git"
		//
		final String originUrl = gitRepository.getConfig().getString("remote", "origin", "url");

		final String url = pom.getUrl();

		final String urlRef;

		if (url != null && url.startsWith("https://maven.avcompris.com/")) {

			urlRef = "https://maven.avcompris.com/" + pom.getArtifactId() + "/";

		} else if (originUrl.startsWith("git@gitlab.com:")) {

			final String suffix = substringAfter(originUrl, ":");

			urlRef = "https://gitlab.com/" + (suffix.endsWith(".git") //
					? substringBeforeLast(suffix, ".git") //
					: suffix);

		} else if (originUrl.startsWith("https://gitlab.com/")) {

			final String suffix = substringAfter(originUrl, "gitlab.com/");

			urlRef = "https://gitlab.com/" + (suffix.endsWith(".git") //
					? substringBeforeLast(suffix, ".git") //
					: suffix);

		} else if (originUrl.startsWith("https://")) {

			assumeTrue(false);

			return;

		} else {

			throw new NotImplementedException("originUrl: " + originUrl);
		}

		assertEquals(removeHttpsCredentials(urlRef), url);
	}

	private static void assumeNotModule(
		final File dir
	) throws IOException {

		if (!new File(dir, ".git").isDirectory()) {

			final File parentDir = dir.getParentFile();

			if (new File(parentDir, ".git").isDirectory()) {

				final File parentPomFile = new File(parentDir, "pom.xml");

				if (parentPomFile.isFile()) {

					final Pom parentPom = DomBinderUtils.xmlContentToJava(parentPomFile, Pom.class);

					assumeFalse(hasModule(parentPom, dir.getName()));
				}
			}
		}
	}

	private static boolean hasModule(
		final Pom pom,
		final String moduleName
	) {

		for (final String module : pom.getModules()) {

			if (module.equals(moduleName)) {

				return true;
			}
		}

		return false;
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testCiManagementSystem(
		final String dirName,
		final File dir,
		final Pom pom
	) throws Exception {

		final CiManagement ciManagement = pom.getCiManagement();

		assumeFalse(ciManagement == null);

		assertEquals("gitlab", ciManagement.getSystem());
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testCiManagementUrl(
		final String dirName,
		final File dir,
		final Pom pom
	) throws Exception {

		final CiManagement ciManagement = pom.getCiManagement();

		assumeFalse(ciManagement == null);

		// e.g. "https://gitlab.com/avcompris/avc-base-testutil"
		//
		final String scmUrl = pom.getScm().getUrl();

		assertEquals(scmUrl + "/-/pipelines", ciManagement.getUrl());
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testCiManagementMeansWeHaveGitLabYml(
		final String dirName,
		final File dir,
		final Pom pom
	) throws Exception {

		final CiManagement ciManagement = pom.getCiManagement();

		assumeFalse(ciManagement == null);

		assertTrue(new File(dir, ".gitlab-ci.yml").isFile(), "The .gitlab-ci.yml file should exist.");
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testGitLabYmlMeansWeHaveCiManagement(
		final String dirName,
		final File dir,
		final Pom pom
	) throws Exception {

		assumeTrue(new File(dir, ".gitlab-ci.yml").isFile());

		assertNotNull(pom.getCiManagement(), "POM should have a <ciManagement> section.");
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testJavadocLinks(
		final String dirName,
		final File dir,
		final Pom pom
	) throws Exception {

		final Plugin javadocPlugin = pom.getPlugin("maven-javadoc-plugin");

		assumeTrue(javadocPlugin != null);

		assertEquals("UTF-8", javadocPlugin.getConfiguration().getEncoding());

		for (final String link : javadocPlugin.getConfiguration().getLinks()) {

			if (!link.startsWith("https://maven.avcompris.com/")) {

				assertEquals("https://docs.oracle.com/javase/8/docs/api/", link);
			}
		}
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testOssrhRepository(
		final String dirName,
		final File dir,
		final Pom pom
	) throws Exception {

		assumeTrue(pom.getModules() == null || pom.getModules().length == 0);

		final Repository repository = pom.getRepository("OSSRH");

		assumeFalse(repository == null);

		assertEquals("https://oss.sonatype.org/content/repositories/snapshots", repository.getUrl());

		assertFalse(repository.hasReleasesEnabled());
		assertTrue(repository.hasSnapshotsEnabled());
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testAvcomprisDependencyVersions(
		final String dirName,
		final File dir,
		final Pom pom
	) throws Exception {

		assumeTrue(pom.getParent() == null //
				|| "net.avcompris.commons".equals(pom.getParent().getGroupId()));

		checkAvcomprisDependencyVersion(pom, pom.getParent());

		for (final Dependency dependency : pom.getDependencies()) {

			checkAvcomprisDependencyVersion(pom, dependency);
		}

		if (!pom.isNullDependencyManagement()) {

			for (final Dependency dependency : pom.getDependencyManagement().getDependencies()) {

				checkAvcomprisDependencyVersion(pom, dependency);
			}
		}
	}

	private static void checkAvcomprisDependencyVersion(
		final Pom pom,
		@Nullable final Dependency dependency
	) {

		if (dependency == null || !"net.avcompris.commons".equals(dependency.getGroupId())) {

			return;
		}

		@Nullable
		final String version = dependency.getVersion();

		if (version == null) {

			return;
		}

		final String artifactId = dependency.getArtifactId();

		@Nullable
		final Pom originalPom = ALL_POMS_BY_DIR_NAMES.get(artifactId);

		if (originalPom == null) {

			return;
		}

		if (version.endsWith("-SNAPSHOT")) {

			assertEquals(originalPom.getVersion(), version,
					pom.getArtifactId() + ".dependency: " + artifactId + ".version");
		}
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testDistributionManagementSite(
		final String dirName,
		final File dir,
		final Pom pom
	) throws Exception {

		assumeTrue(pom.getModules() == null || pom.getModules().length == 0);

		final String groupId = pom.isNullGroupId() //
				? pom.getParent().getGroupId() //
				: pom.getGroupId();

		assumeTrue("net.avcompris.commons".equals(groupId));

		assumeFalse(Lists.newArrayList( //
				"guixer-getting-started" // No generated site: Keep POM simple
		).contains(pom.getArtifactId()));

		final String projectName = pom.getName();

		final Site site = pom.getDistributionManagement().getSite();

		assertEquals("avcompris-sites", site.getId());

		assertEquals("scp://maven.avcompris.com/" + projectName + "/", site.getUrl());
	}

	private static final Map<String, Pom> ALL_POMS_BY_DIR_NAMES = newHashMap();

	private static List<Arguments> DIRS_AND_POMS = null;

	private static Stream<Arguments> dirsAndPoms() throws Exception {

		if (DIRS_AND_POMS != null) {

			return DIRS_AND_POMS.stream();
		}

		final List<Arguments> arguments = newArrayList();

		Workspace.scanForPoms(new File("../"), (
			dir,
			pom) -> {

			final String dirName = dir.getName();

			arguments.add(Arguments.of(dirName, dir, pom));

			ALL_POMS_BY_DIR_NAMES.put(dirName, pom);
		});

		DIRS_AND_POMS = arguments;

		return arguments.stream();
	}
}
