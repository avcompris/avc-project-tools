package net.avcompris.devops;

import java.io.File;
import java.util.List;

import org.junit.jupiter.api.Test;

import net.avcompris.project.Pom;

public class PomParentsHtmlGeneratorTest {

	@Test
	public void testPomParentsHtmlGenerator() throws Exception {
		
		final List<Pom> poms = Workspace.getAllPoms(new File(".."));
		
		final PomParentsHtmlGenerator generator = new PomParentsHtmlGenerator(poms);
		
		generator.generateHTML(new File("target","pomParents.html"));
	}
}
