<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="UTF-8"/>

<xsl:template match="/workspace">

<xsl:variable name="poms" select="projects/project"/>
<xsl:variable name="pomCount" select="count($poms)"/>
<xsl:variable name="parents" select="parents/parent"/>

<html>
<head>
<title>
	pomParents: <xsl:value-of select="$pomCount"/> projects
</title>
</head>
<style type="text/css">
body {
	font-family: Arial, Helvetica, sans-serif;
}
h1 {
	text-align: center;
}
table {
	border-collapse: collapse;
	font-size: small;
	margin: auto;
}
thead {
	background-color: #ddd;
}
tr.odd {
	background-color: #ddf;
}
tbody tr:hover {
	background-color: #aad;
}
tr.noParent th {
	color: #aaa;
}
th,
td {
	border: 1px solid #000;
	padding: 0.2em 0.4em;
	vertical-align: top;
}
thead th.projectName,
tbody th {
	text-align: left;
}
thead th,
tbody td {
	text-align: center;
}
thead th.distributionManagementSnapshotRepositoryIsOssrh,
tr.distributionManagementSnapshotRepositoryIsOssrh th,
tr.distributionManagementSnapshotRepositoryIsOssrh td.distributionManagementSnapshotRepositoryIsOssrh,
tr.noParent.distributionManagementSnapshotRepositoryIsOssrh th {
	color: #f00;
}
</style>
<body>

<h1>
	pomParents: <xsl:value-of select="$pomCount"/> projects
</h1>

<table>
<thead>
<tr>
	<th class="projectName">project / parent</th>
	
	<xsl:for-each select="$parents">
	<xsl:variable name="artifactId" select="artifactId"/>
	<xsl:variable name="parentPom" select="$poms[artifactId = $artifactId]"/>
	<th>
	<xsl:attribute name="class">
	<xsl:if test="$parentPom/distributionManagement/snapshotRepository/id = 'OSSRH'">
		distributionManagementSnapshotRepositoryIsOssrh
	</xsl:if>
	</xsl:attribute>
	<xsl:if test="$parentPom/distributionManagement/snapshotRepository/id = 'OSSRH'">
	<xsl:attribute name="title">
		<xsl:text>Distribution Management Snapshot Repository is: OSSRH</xsl:text>
	</xsl:attribute>
	</xsl:if>
	
		<xsl:value-of select="$artifactId"/>
	</th>
	</xsl:for-each>
</tr>
</thead>
<tbody>
<xsl:for-each select="$poms">
<xsl:variable name="parent" select="parent"/>
<xsl:variable name="distributionManagementSnapshotRepositoryId">
	<xsl:choose>
	<xsl:when test="distributionManagement/snapshotRepository">
		<xsl:value-of select="distributionManagement/snapshotRepository/id"/>
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="$poms[artifactId = $parent/artifactId]
			/distributionManagement/snapshotRepository/id"/>
	</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<tr>
<xsl:attribute name="class">
	<xsl:choose>
	<xsl:when test="position() mod 2 = 1">odd</xsl:when>
	<xsl:otherwise>even</xsl:otherwise>
	</xsl:choose>
	<xsl:if test="not($parent)"> noParent</xsl:if>
	<xsl:if test="$distributionManagementSnapshotRepositoryId = 'OSSRH'">
		distributionManagementSnapshotRepositoryIsOssrh
	</xsl:if>
</xsl:attribute>
	<th class="projectName">
	<xsl:if test="$distributionManagementSnapshotRepositoryId = 'OSSRH'">
	<xsl:attribute name="title">
		<xsl:text>Distribution Management Snapshot Repository is: OSSRH</xsl:text>
	</xsl:attribute>
	</xsl:if>
		<xsl:value-of select="artifactId"/>
	</th>
	<xsl:for-each select="$parents/artifactId">
	<xsl:variable name="artifactId" select="."/>
	<xsl:variable name="parentPom" select="$poms[artifactId = $artifactId]"/>
	<xsl:variable name="parentPomDistributionManagementSnapshotRepositoryId"
		select="$parentPom/distributionManagement/snapshotRepository/id"/>
	<td>
	<xsl:if test="$artifactId = $parent/artifactId">
	<xsl:attribute name="class">
		<xsl:choose>
		<xsl:when test="$distributionManagementSnapshotRepositoryId = 'OSSRH'
			and $parentPomDistributionManagementSnapshotRepositoryId = 'OSSRH'">
				distributionManagementSnapshotRepositoryIsOssrh
		</xsl:when>
		</xsl:choose>
	</xsl:attribute>
	<xsl:attribute name="title">
		<xsl:value-of select="concat($parent/artifactId, ':', $parent/version)"/>
		<xsl:choose>
		<xsl:when test="$distributionManagementSnapshotRepositoryId = 'OSSRH'
				and $parentPomDistributionManagementSnapshotRepositoryId = 'OSSRH'">
			<xsl:text>&#10;Distribution Management Snapshot Repository is: OSSRH</xsl:text>
		</xsl:when>
		<xsl:when test="not($distributionManagementSnapshotRepositoryId = 'OSSRH')
				and $parentPomDistributionManagementSnapshotRepositoryId = 'OSSRH'">
			<xsl:text>&#10;Parent Distribution Management Snapshot Repository is: OSSRH, but is overriden</xsl:text>
		</xsl:when>
		</xsl:choose>
	</xsl:attribute>
			<xsl:value-of select="concat($parent/version)"/>
	</xsl:if>
	</td>
	</xsl:for-each>
</tr>
</xsl:for-each>
</tbody>
</table>

</body>
</html>

</xsl:template>

</xsl:stylesheet>
