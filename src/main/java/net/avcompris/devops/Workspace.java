package net.avcompris.devops;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import net.avcompris.binding.dom.helper.DomBinderUtils;
import net.avcompris.project.Pom;

public class Workspace {

	public static void scanForPoms(final File workspaceDir, final BiConsumer<File, Pom> consumer) throws IOException {

		checkNotNull(workspaceDir, "workspaceDir");
		checkNotNull(consumer, "consumer");

		new Workspace(consumer).scan(workspaceDir);
	}

	public static List<Pom> getAllPoms(final File workspaceDir) throws IOException {

		checkNotNull(workspaceDir, "workspaceDir");

		final List<Pom> allPoms = newArrayList();

		new Workspace((dir, pom) -> {

			allPoms.add(pom);

		}).scan(workspaceDir);

		return allPoms;
	}

	private void scanPom(final File dir, final Pom pom) throws IOException {

		final String dirName = dir.getName();

		if (allPomsByDirNames.containsKey(dirName)) {

			// throw new IllegalStateException("Directory name was encountered twice: " + dirName);

			System.err.println("WARNING *** Directory name was encountered twice: " + dirName);

			return;
		}

		System.out.println("Adding: " + dirName + "...");

		allPomsByDirNames.put(dirName, pom);

		consumer.accept(dir, pom);

		for (final String module : pom.getModules()) {

			final File moduleDir = new File(dir, module);

			if (!moduleDir.isDirectory()) {

				continue;

				// throw new FileNotFoundException(moduleDir.getCanonicalPath());
			}

			final File modulePomFile = new File(moduleDir, "pom.xml");

			if (!modulePomFile.isFile()) {

				throw new FileNotFoundException(modulePomFile.getCanonicalPath());
			}

			final Pom modulePom = DomBinderUtils.xmlContentToJava(modulePomFile, Pom.class);

			scanPom(moduleDir, modulePom);
		}
	}

	private final Map<String, Pom> allPomsByDirNames = newHashMap();

	private final BiConsumer<File, Pom> consumer;

	private Workspace(final BiConsumer<File, Pom> consumer) {

		this.consumer = checkNotNull(consumer, "consumer");
	}

	private void scan(final File workspaceDir) throws IOException {

		for (final File dir : workspaceDir.listFiles(file -> file.isDirectory())) {

			final File pomFile = new File(dir, "pom.xml");

			if (!pomFile.isFile()) {

				continue;
			}

			final Pom pom = DomBinderUtils.xmlContentToJava(pomFile, Pom.class);

			scanPom(dir, pom);
		}
	}
}
