package net.avcompris.devops;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newTreeSet;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.commons.lang3.StringUtils.substringBeforeLast;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Comparator;

import javax.annotation.Nullable;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import net.avcompris.domdumper.Dumper;
import net.avcompris.domdumper.XMLDumpers;
import net.avcompris.project.Pom;
import net.avcompris.project.Pom.DistributionManagement;
import net.avcompris.project.Pom.DistributionRepository;
import net.avcompris.project.Pom.Parent;

public class PomParentsHtmlGenerator {

	final Collection<Pom> poms;

	public PomParentsHtmlGenerator(final Collection<Pom> poms) {

		this.poms = checkNotNull(poms, "poms");
	}

	public void generateHTML(final File htmlOutFile) throws IOException, TransformerException {

		checkNotNull(htmlOutFile, "htmlOutFile");

		final File xmlFile = new File(htmlOutFile.getParentFile(),
				substringBeforeLast(htmlOutFile.getName(), ".") + ".xml");

		generateXML(xmlFile);

		final Transformer transformer = TransformerFactory.newInstance().newTransformer( //
				new StreamSource(new File("src/main/resources", "xslt/pomParents.html.xsl")));

		transformer.transform( //
				new StreamSource(xmlFile), //
				new StreamResult(htmlOutFile));
	}

	public void generateXML(final File xmlOutFile) throws IOException, TransformerException {

		checkNotNull(xmlOutFile, "xmlOutFile");

		final Collection<Pom> sortedPoms = newTreeSet(new Comparator<Pom>() {

			@Override
			public int compare(final Pom p1, final Pom p2) {

				return p1.getArtifactId().compareTo(p2.getArtifactId());
			}
		});

		sortedPoms.addAll(poms);

		final Collection<String> sortedParentArtifactIds = newTreeSet();

		final Dumper dumper = XMLDumpers.newDumper("workspace", xmlOutFile, UTF_8.name());

		final Dumper projectsDumper = dumper.addElement("projects");

		for (final Pom pom : sortedPoms) {

			final Dumper projectDumper = projectsDumper.addElement("project");

			projectDumper.addElement("artifactId").addCharacters(pom.getArtifactId());

			final Parent parent = pom.getParent();

			if (parent != null) {

				final Dumper parentDumper = projectDumper.addElement("parent");

				final String parentArtifactId = parent.getArtifactId();

				parentDumper.addElement("groupId").addCharacters(parent.getGroupId());
				parentDumper.addElement("artifactId").addCharacters(parentArtifactId);
				parentDumper.addElement("version").addCharacters(parent.getVersion());

				sortedParentArtifactIds.add(parentArtifactId);
			}

			if (!pom.isNullDistributionManagement()) {

				final DistributionManagement distributionManagement = pom.getDistributionManagement();

				final Dumper distributionManagementDumper = projectDumper.addElement("distributionManagement");

				dump(distributionManagementDumper, distributionManagement.getSnapshotRepository());

				dump(distributionManagementDumper, distributionManagement.getRepository());
			}
		}

		final Dumper parentsDumper = dumper.addElement("parents");

		for (final String parentArtifactId : sortedParentArtifactIds) {

			final Dumper parentDumper = parentsDumper.addElement("parent");

			parentDumper.addElement("artifactId").addCharacters(parentArtifactId);
		}

		dumper.close();
	}

	private static void dump(final Dumper dumper, @Nullable final DistributionRepository repository)
			throws IOException {

		if (repository == null) {
			return;
		}

		final Dumper repositoryDumper = dumper.addElement(repository.getElementName());

		repositoryDumper.addElement("id").addCharacters(repository.getId());
	}
}
