# About avc-project-tools

These are some
Avantage Compris’ public tools
for projects, especially Maven projects.

[API Documentation is here](https://maven.avcompris.com/avc-project-tools/apidocs/index.html).

There is a [Maven Generated Site.](https://maven.avcompris.com/avc-project-tools/)

## Usage

Your projects must all be at the same level
in your workspace. They may reference modules
elsewhere though.

Run the Java test classes in the project.

## Checks

The test classes perform the following sanity checks:


### Checks on Maven projects

* All declared `<scm>` blocks in POMs match the Git actual `origin` remote;
* Directory names match `artifactId` in POMs;


